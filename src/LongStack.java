import java.util.LinkedList;
import java.util.StringTokenizer;

public class LongStack {

   private LinkedList<Long> linkedlist;

   public static void main (String[] argum) {

      System.out.println(interpret("8   8 - 11 +"));

   }

   LongStack() {
      this.linkedlist = new LinkedList<>();

   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      LongStack m = new LongStack();
      for (Long aLong : this.linkedlist) {
         m.push(aLong);
      }
      return m;
   }

   public boolean stEmpty() {

      return linkedlist.size() == 0;
   }

   public void push (long a) {

   this.linkedlist.addLast(a);
   }

   public long pop() {
      if (stEmpty()){
         throw new RuntimeException("Stack is empty");
      }
      return this.linkedlist.removeLast();
   } // pop

   public void op (String s) {
      if (this.linkedlist.size() < 2){
         throw new IndexOutOfBoundsException(" too few elements for " + s + toString());
      }
      Long op2 = pop();
      Long op1 = pop();
      switch (s) {
         case "+":
            push(op1 + op2);
            break;
         case "-":
            push(op1 - op2);
            break;
         case "*":
            push(op1 * op2);
            break;
         case "/":
            push(op1 / op2);
            break;
         case "SWAP":
            push(op2);
            push(op1);
            break;
         case "ROT":
            Long op3 = pop();
            push(op1);
            push(op2);
            push(op3);
            break;
         default:
            throw new IllegalArgumentException("Invalid operation: " + s);
      }

   }
  
   public long tos() {
      if (stEmpty()){
         throw new RuntimeException("List is empty");
      }
      return this.linkedlist.getLast();
   }

   @Override
   public boolean equals (Object o) {
      if (((LongStack) o).linkedlist.size() != this.linkedlist.size()){
         return false;
      }
      else {
         for (int i = 0; i < linkedlist.size(); i++) {
            if (!this.linkedlist.get(i).equals(((LongStack) o).linkedlist.get(i))){
               return false;
            }

         }
      }
      return true;
   }

   @Override
   public String toString() {
      if (stEmpty()){
         return "";
      }
      return String.valueOf(this.linkedlist);
   }

   public static long interpret (String pol) {
      if (pol.length() == 0){
         throw new RuntimeException("Empty expression");
      }
      LongStack sum = new LongStack();
      StringTokenizer array = new StringTokenizer(pol);
      while (array.hasMoreElements()) {
         String element = String.valueOf(array.nextElement());
         try{
            long i = Long.parseLong(element);
            sum.push(i);
         }catch (Exception e){
            if (sum.linkedlist.size() < 2){
               throw new RuntimeException("Cannot perform " + element + " in expression " + pol);
            }else if (!element.equals("+") && !element.equals("-") && !element.equals("*") && !element.equals("/") && !element.equals("SWAP") && !element.equals("ROT")){
               throw new RuntimeException("Illegal symbol " + element + " in expression " + pol);
            }
            else {
               sum.op(element);
            }
         }
      }
      if (sum.linkedlist.size() > 1){
         throw new RuntimeException("Too many numbers in expression " + pol);
      }
      return sum.pop();
   }

}

